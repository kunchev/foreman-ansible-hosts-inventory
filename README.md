# foreman-ansible-hosts-inventory
 - Parse the Foreman API and print all environments with their respective IDs
 - Parse the Foreman API per environment and generate Ansible hosts file containing host group and hosts related
 - Cron job can be set to automatically generate recent hosts file

# example usage
```
# display help
python3 generate_hosts.py -h [--help]

# make sure that file is executable if calling without 'python3'
./generate_hosts.py -h
./generate_hosts.py --help

# list foreman environments with respective IDs
python3 generate_hosts.py --action listenvs
python3 generate_hosts.py -a listenvs

# parse selected foreman environment and generate ansible hosts file
python3 generate_hosts.py --action parseenv 1
python3 generate_hosts.py -a parseenv 1
```

# example generated file
Each host group in selected Foreman environment is represented in the file below in square brackets with all servers contained  
*lets open example file, generated by the script from a test Foreman instance*
```
 - vim ./foreman_hosts
 - nano ./foreman_hosts
 - cat ./foreman_hosts
 - less ./foreman_hosts
 # or use any other method to list the contents of the file
```

```
# Inventory file generated on 02/12/2020 16:19:39

[production/Web]
webserver01.domain.a
webserver02.domain.a

[test/DB]
dbserver01.domain.b
dbserver02.domain.b

[development]
devbox01.domain.a
devbox01.domain.a

[applicationA/Dev/locationA]
appsrv01.domain.c
appsrv02.domain.c

[demo/DB/locationB]
demo-dbsrv01.domain.d
demo-dbsrv02.domain.d
demo-dbsrv03.domain.d

[mailservers]
mail01.example.com
mail02.example.com

[backupservers]
one-backup.example.com
two-backup.example.com
three-backup.example.com
```

 - This inventory file can be passed to Ansible and be to be used as a hosts file
```
# Use Ansible 'raw' module to execute the 'uptime' command on all servers in the 'test/DB' hostgroup
ansible -m raw -a "uptime" -i /path/to/foreman_inventory test/DB

# Use Ansible 'raw' module to execute the 'hostname -s' command on all servers in the 'mailservers' and 'backupservers' hostgroups
ansible -m raw -a "hostname -s" /path/to/foreman_inventory mailservers:backupservers
```
 - Any other Ansible module can be used as well as playbook or role, all you need to do is point the path to the 'foreman_hosts' file and specify the desired host group(s), or simply set 'all' to run on all host groups in the file
