#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# TODO: implement proper exception handling and meaningful error codes
# TODO: implement zip method to be reused in parse_envs() and parse_hosts()
# TODO: implement color prints


__author__ = 'Petyo Kunchev'
__version__ = '1.2.4'
__maintainer__ = 'Petyo Kunchev'
__license__ = 'MIT'


import os
from typing import Any
from datetime import datetime

try:
    import json
    import urllib3
    import argparse
    import configparser
    import requests
    from requests.models import Response
    from collections import defaultdict
except ModuleNotFoundError as err:
    print('pip3 install -r requirements.txt')
    exit(err)


# Disable the warnings for the self-signed SSL certificates
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


def read_settings():
    """
    Read the settings from the foreman.ini external configuration file.

    :rtype: dict
    :return: parsed - supplied script ini config file data
    """
    config = configparser.ConfigParser()
    scriptdir: str = os.path.dirname(os.path.realpath(__file__))
    foreman_default_ini_path: str = os.path.join(scriptdir, 'foreman.ini')
    foreman_ini_path: str = os.environ.get('FOREMAN_INI_PATH',
                                           foreman_default_ini_path)
    config.read(foreman_ini_path)
    settings: dict = {}
    required: list = ['base_url', 'username', 'password', 'hfile']
    missing: list = []

    setting: str
    for setting in required:
        value = config.get('foreman', setting)
        if value is None:
            missing.append(setting)
        settings[setting] = value
    if missing:
        exit('No values, please check your ini file!')

    return settings


def parse_args():
    """
    Arguments parser function, based on the argparse module.

    To list all Foreman environments: '[-a | --action] listenvs'
    To parse a desired environment: '[-a | --action] parseenv -e <envid>

    :rtype: object
    :return: parsed - supplied script arguments
    """
    # crete parser object
    parser = argparse.ArgumentParser(
        prog='Foreman Ansible Hosts Inventory Parser/Generator',
        description='Parse Foreman API environments and hosts per environment',
        epilog='Enjoy!')

    # add argument for the main action which will be taken
    parser.add_argument('--action', '-a', metavar='<action>',
                        choices=['listenvs', 'parseenv'],
                        action='store', dest="action", default="listenvs",
                        help='[listenvs, parseenv] - the action that will be '
                             'taken. "listenvs" will list all Foreman '
                             'environments with respective IDs. "parseenv" '
                             'will parse the selected/supplied environment ID '
                             'and generate proper Ansible inventory file.')

    # add argument for supplying the desired environment ID for parsing
    parser.add_argument('--environment', '-e', metavar='<environment>',
                        action='store',
                        dest="environment",
                        help='Specifies the Foreman environment ID which '
                             'will be parsed, example: 1, the number matches '
                             'the environment name from the list.')

    # parse args passed on the cli
    args = parser.parse_args()

    return args


class AnsibleInventory(object):
    """
    AnsibleInventory class.

    This class is used to represent an Ansible hosts file generation,
    based on Foreman configuration. Generated servers in the hosts
    file must already be members of any Foreman environment.

    Methods
    -------
    parse_envs()
        Parse the Foreman API and print the environments configured
    parse_hosts(environment_id: str = None)
        Parse the Foreman API per env and generate Ansible hosts file
    """

    def __init__(self, base_url, username, password, hostfile):
        """
        Class constrictor.

        :type base_url: str: Base Foreman API URL
        :type username: str: Foreman API username
        :type password: str: The password for the API username
        :type hostfile: str: Output file where data will be stored
        """
        self.base_url: str = base_url
        self.username: str = username
        self.password: str = password
        self.hfile: str = hostfile

    def parse_envs(self):
        """
        Parse the Foreman API, collect and print data for the currently
        present and configured environments: env name and env ID.

        """
        # API parse related
        r: Response = requests.get(self.base_url,
                                   auth=(self.username, self.password),
                                   verify=False)
        response: str = r.text
        env_data: dict = json.loads(response)
        data: dict = env_data['results']  # get data from the results key
        parsed_envs: list = []  # define list to append env names to
        parsed_env_ids: list = []  # define list to append env IDs to
        results_dict: defaultdict[Any, list] = defaultdict(list)

        # append env names and env IDs to their respective lists
        for data in data:
            env_name: object = data['name']  # filter environment name
            env_id: object = data['id']  # get environment ID
            parsed_envs.append(env_name)
            parsed_env_ids.append(env_id)

        # zip the two lists into single defaultdict
        for ename, eid in zip(parsed_envs, parsed_env_ids):
            results_dict[ename].append(eid)

        # display the results
        print('-List with all Foreman environments with their respective IDs-')
        [print(f'{n:<15} {i}') for n, i in results_dict.items()]

    def parse_hosts(self, environment_id: str = None):
        """
        Parse the Foreman API per env and generate Ansible hosts.

        When ran against a specific foreman environment ID, this script
        generates Ansible hosts file, based on the obtained from Foreman
        data.

        :type environment_id: Foreman environment ID provided as arg
        """
        # construct the Foreman API URL for the selected environment -
        # results are limited up to 100000, change accordingly
        url: str = f'{self.base_url + environment_id}/hosts?per_page=100000'

        print('Starting hosts file generation, please wait...')

        # generate time stamp for the hosts file header comments
        now = datetime.now()
        fdate = now.strftime('%d/%m/%Y %H:%M:%S')

        print(f'Parsing Foreman environment with id: {environment_id}')

        # API parse related
        r: Response = requests.get(url, auth=(self.username, self.password),
                                   verify=False)
        response: str = r.text
        foreman_data: dict = json.loads(response)
        data = foreman_data['results']  # get data from the results key
        parsed_groups: list = []  # define list to append groups to
        parsed_hosts: list = []  # define list to append hosts to
        results_dict: defaultdict[Any, list] = defaultdict(list)

        # append groups and hosts to their respective lists
        for data in data:
            host_group: object = data['hostgroup_title']  # host groups
            name: object = data['name']  # servers in each host group
            parsed_groups.append(host_group)
            parsed_hosts.append(name)

        # zip the two lists into single defaultdict
        for group, host in zip(parsed_groups, parsed_hosts):
            results_dict[group].append(host)

        # write the results to the desired file
        with open(self.hfile, 'w') as hosts:
            hosts.write(f'# Inventory file generated on {fdate}.\n')
            for key in results_dict:
                hosts.write(f'\n[{key}]\n')
                for val in results_dict[key]:
                    hosts.write(f'{val}\n')

        print('Ansible hosts file generation has completed.')


def main():
    """
    The main function, wrapping up the logic.

    Parse the Foreman API, list all environments, generate Ansible
    inventory hosts file.
    """
    # create objects for the arguments, config settings and inventory
    settings: dict = read_settings()
    arguments = parse_args()
    hosts = AnsibleInventory(settings['base_url'], settings['username'],
                             settings['password'], settings['hfile'])

    # if 'listenvs' is supplied as action, call parse_envs() method
    if arguments.action == 'listenvs':
        hosts.parse_envs()

    # if 'parseenv' is supplied as action, call parse_hosts(arg) method
    if arguments.action == 'parseenv':
        if arguments.environment is not None:
            envid = arguments.environment
            hosts.parse_hosts(envid)
        else:
            exit('f Please provide correct environment id: {envid} provided. '
                 'List all environments supplying the <-a listenvs> '
                 'arguments to the script.')


if __name__ == '__main__':
    main()
